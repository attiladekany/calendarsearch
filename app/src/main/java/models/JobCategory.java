package models;

public class JobCategory
{
    private String Code;

    private String Name;

    public String getCode ()
    {
        return Code;
    }

    public void setCode (String Code)
    {
        this.Code = Code;
    }

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Code = "+Code+", Name = "+Name+"]";
    }
}
