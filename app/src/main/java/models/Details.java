package models;

public class Details
{
    private String JobSummary;

    private String PromotionPotential;

    private String DetailStatusUrl;

    private String PromotionPotentialAdditionalText;

    private String[] HiringPath;

    private WhoMayApply WhoMayApply;

    private String AppointmentExplanationText;

    private String HighGrade;

    private String LowGrade;

    private String TravelCode;

    private String ApplyOnlineUrl;

    private String AgencyMarketingStatement;

    public String getJobSummary ()
    {
        return JobSummary;
    }

    public void setJobSummary (String JobSummary)
    {
        this.JobSummary = JobSummary;
    }

    public String getPromotionPotential ()
    {
        return PromotionPotential;
    }

    public void setPromotionPotential (String PromotionPotential)
    {
        this.PromotionPotential = PromotionPotential;
    }

    public String getDetailStatusUrl ()
    {
        return DetailStatusUrl;
    }

    public void setDetailStatusUrl (String DetailStatusUrl)
    {
        this.DetailStatusUrl = DetailStatusUrl;
    }

    public String getPromotionPotentialAdditionalText ()
    {
        return PromotionPotentialAdditionalText;
    }

    public void setPromotionPotentialAdditionalText (String PromotionPotentialAdditionalText)
    {
        this.PromotionPotentialAdditionalText = PromotionPotentialAdditionalText;
    }

    public String[] getHiringPath ()
    {
        return HiringPath;
    }

    public void setHiringPath (String[] HiringPath)
    {
        this.HiringPath = HiringPath;
    }

    public WhoMayApply getWhoMayApply ()
    {
        return WhoMayApply;
    }

    public void setWhoMayApply (WhoMayApply WhoMayApply)
    {
        this.WhoMayApply = WhoMayApply;
    }

    public String getAppointmentExplanationText ()
    {
        return AppointmentExplanationText;
    }

    public void setAppointmentExplanationText (String AppointmentExplanationText)
    {
        this.AppointmentExplanationText = AppointmentExplanationText;
    }

    public String getHighGrade ()
    {
        return HighGrade;
    }

    public void setHighGrade (String HighGrade)
    {
        this.HighGrade = HighGrade;
    }

    public String getLowGrade ()
    {
        return LowGrade;
    }

    public void setLowGrade (String LowGrade)
    {
        this.LowGrade = LowGrade;
    }

    public String getTravelCode ()
    {
        return TravelCode;
    }

    public void setTravelCode (String TravelCode)
    {
        this.TravelCode = TravelCode;
    }

    public String getApplyOnlineUrl ()
    {
        return ApplyOnlineUrl;
    }

    public void setApplyOnlineUrl (String ApplyOnlineUrl)
    {
        this.ApplyOnlineUrl = ApplyOnlineUrl;
    }

    public String getAgencyMarketingStatement ()
    {
        return AgencyMarketingStatement;
    }

    public void setAgencyMarketingStatement (String AgencyMarketingStatement)
    {
        this.AgencyMarketingStatement = AgencyMarketingStatement;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [JobSummary = "+JobSummary+", PromotionPotential = "+PromotionPotential+", DetailStatusUrl = "+DetailStatusUrl+", PromotionPotentialAdditionalText = "+PromotionPotentialAdditionalText+", HiringPath = "+HiringPath+", WhoMayApply = "+WhoMayApply+", AppointmentExplanationText = "+AppointmentExplanationText+", HighGrade = "+HighGrade+", LowGrade = "+LowGrade+", TravelCode = "+TravelCode+", ApplyOnlineUrl = "+ApplyOnlineUrl+", AgencyMarketingStatement = "+AgencyMarketingStatement+"]";
    }
}