package com.example.calendarsearch;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import models.SearchResultItems;

public class JobAdapter extends RecyclerView.Adapter<JobViewHolder> implements Filterable, IAdapterInterface<SearchResultItems> {
    private ArrayList<SearchResultItems> jobList;
    private ArrayList<SearchResultItems> jobListFull;

    JobAdapter(ArrayList<SearchResultItems> jobList) {
        this.jobList = jobList;
        jobListFull = new ArrayList<>(jobList);
    }

    public void updateAdapter(ArrayList<SearchResultItems> newExampleList){
        jobList = newExampleList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public JobViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.example_item,
                parent, false);
        return new JobViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull JobViewHolder holder, int position) {
        SearchResultItems currentItem = jobList.get(position);

        holder.PositionID.setText(currentItem.getMatchedObjectDescriptor().getPositionID());
        holder.PositionTitle.setText(currentItem.getMatchedObjectDescriptor().getPositionTitle());
        holder.JobCategory_Name.setText(currentItem.getMatchedObjectDescriptor().getJobCategory()[0].getName());
        holder.PositionLocationDisplay.setText((currentItem.getMatchedObjectDescriptor().getPositionLocationDisplay()));
        holder.PositionOfferingType.setText(currentItem.getMatchedObjectDescriptor().getPositionOfferingType()[0].getName());
        holder.PositionSchedule_Name.setText(currentItem.getMatchedObjectDescriptor().getPositionSchedule()[0].getName());
        holder.PositionRemuneration_MinimumRange.setText(currentItem.getMatchedObjectDescriptor().getPositionRemuneration()[0].getMinimumRange());
        holder.PositionRemuneration_MaximumRange.setText(currentItem.getMatchedObjectDescriptor().getPositionRemuneration()[0].getMaximumRange());
    }

    @Override
    public int getItemCount() {
        return jobList.size();
    }

    @Override
    public Filter getFilter() {
        return exampleFilter;
    }

    private Filter exampleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            //Todo: implement filtering in the other fields
            ArrayList<SearchResultItems> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(jobListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (SearchResultItems item : jobListFull) {
                    if (item.getMatchedObjectDescriptor().getPositionID().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            jobList.clear();
            jobList.addAll((List<SearchResultItems>) results.values);
            notifyDataSetChanged();
        }
    };
}