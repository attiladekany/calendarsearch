package models;

public class SearchResultItems
{
    private MatchedObjectDescriptor MatchedObjectDescriptor;

    private String RelevanceRank;

    private String MatchedObjectId;

    public MatchedObjectDescriptor getMatchedObjectDescriptor ()
    {
        return MatchedObjectDescriptor;
    }

    public void setMatchedObjectDescriptor (MatchedObjectDescriptor MatchedObjectDescriptor)
    {
        this.MatchedObjectDescriptor = MatchedObjectDescriptor;
    }

    public String getRelevanceRank ()
    {
        return RelevanceRank;
    }

    public void setRelevanceRank (String RelevanceRank)
    {
        this.RelevanceRank = RelevanceRank;
    }

    public String getMatchedObjectId ()
    {
        return MatchedObjectId;
    }

    public void setMatchedObjectId (String MatchedObjectId)
    {
        this.MatchedObjectId = MatchedObjectId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [MatchedObjectDescriptor = "+MatchedObjectDescriptor+", RelevanceRank = "+RelevanceRank+", MatchedObjectId = "+MatchedObjectId+"]";
    }
}