package models;

public class PositionRemuneration
{
    private String MaximumRange;

    private String MinimumRange;

    private String RateIntervalCode;

    public String getMaximumRange ()
    {
        return MaximumRange;
    }

    public void setMaximumRange (String MaximumRange)
    {
        this.MaximumRange = MaximumRange;
    }

    public String getMinimumRange ()
    {
        return MinimumRange;
    }

    public void setMinimumRange (String MinimumRange)
    {
        this.MinimumRange = MinimumRange;
    }

    public String getRateIntervalCode ()
    {
        return RateIntervalCode;
    }

    public void setRateIntervalCode (String RateIntervalCode)
    {
        this.RateIntervalCode = RateIntervalCode;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [MaximumRange = "+MaximumRange+", MinimumRange = "+MinimumRange+", RateIntervalCode = "+RateIntervalCode+"]";
    }
}