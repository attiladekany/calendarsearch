package com.example.calendarsearch;

import java.util.ArrayList;
import java.util.List;

import models.RootObject;
import models.SearchResultItems;

public class JobSearchResponseMapper {
    public List<SearchResultItems> mapToSearchItems(RootObject response) {
        List<SearchResultItems> searchItems = new ArrayList<>();
        for (SearchResultItems item : response.getSearchResult().getSearchResultItems()) {
            searchItems.add(item);
        }

        return searchItems;
    }
}
