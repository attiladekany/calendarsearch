package models;

public class MatchedObjectDescriptor
{
    private String OrganizationName;

    private String PositionLocationDisplay;

    private String PositionTitle;

    private PositionSchedule[] PositionSchedule;

    private String ApplicationCloseDate;

    private String PositionID;

    private String PositionEndDate;

    private PositionOfferingType[] PositionOfferingType;

    private PositionRemuneration[] PositionRemuneration;

    private String[] ApplyURI;

    private PositionFormattedDescription[] PositionFormattedDescription;

    private PositionLocation[] PositionLocation;

    private String QualificationSummary;

    private String PositionStartDate;

    private UserArea UserArea;

    private JobCategory[] JobCategory;

    private String DepartmentName;

    private JobGrade[] JobGrade;

    private String PositionURI;

    private String PublicationStartDate;

    public String getOrganizationName ()
    {
        return OrganizationName;
    }

    public void setOrganizationName (String OrganizationName)
    {
        this.OrganizationName = OrganizationName;
    }

    public String getPositionLocationDisplay ()
    {
        return PositionLocationDisplay;
    }

    public void setPositionLocationDisplay (String PositionLocationDisplay)
    {
        this.PositionLocationDisplay = PositionLocationDisplay;
    }

    public String getPositionTitle ()
    {
        return PositionTitle;
    }

    public void setPositionTitle (String PositionTitle)
    {
        this.PositionTitle = PositionTitle;
    }

    public PositionSchedule[] getPositionSchedule ()
    {
        return PositionSchedule;
    }

    public void setPositionSchedule (PositionSchedule[] PositionSchedule)
    {
        this.PositionSchedule = PositionSchedule;
    }

    public String getApplicationCloseDate ()
    {
        return ApplicationCloseDate;
    }

    public void setApplicationCloseDate (String ApplicationCloseDate)
    {
        this.ApplicationCloseDate = ApplicationCloseDate;
    }

    public String getPositionID ()
    {
        return PositionID;
    }

    public void setPositionID (String PositionID)
    {
        this.PositionID = PositionID;
    }

    public String getPositionEndDate ()
    {
        return PositionEndDate;
    }

    public void setPositionEndDate (String PositionEndDate)
    {
        this.PositionEndDate = PositionEndDate;
    }

    public PositionOfferingType[] getPositionOfferingType ()
    {
        return PositionOfferingType;
    }

    public void setPositionOfferingType (PositionOfferingType[] PositionOfferingType)
    {
        this.PositionOfferingType = PositionOfferingType;
    }

    public PositionRemuneration[] getPositionRemuneration ()
    {
        return PositionRemuneration;
    }

    public void setPositionRemuneration (PositionRemuneration[] PositionRemuneration)
    {
        this.PositionRemuneration = PositionRemuneration;
    }

    public String[] getApplyURI ()
    {
        return ApplyURI;
    }

    public void setApplyURI (String[] ApplyURI)
    {
        this.ApplyURI = ApplyURI;
    }

    public PositionFormattedDescription[] getPositionFormattedDescription ()
    {
        return PositionFormattedDescription;
    }

    public void setPositionFormattedDescription (PositionFormattedDescription[] PositionFormattedDescription)
    {
        this.PositionFormattedDescription = PositionFormattedDescription;
    }

    public PositionLocation[] getPositionLocation ()
    {
        return PositionLocation;
    }

    public void setPositionLocation (PositionLocation[] PositionLocation)
    {
        this.PositionLocation = PositionLocation;
    }

    public String getQualificationSummary ()
    {
        return QualificationSummary;
    }

    public void setQualificationSummary (String QualificationSummary)
    {
        this.QualificationSummary = QualificationSummary;
    }

    public String getPositionStartDate ()
    {
        return PositionStartDate;
    }

    public void setPositionStartDate (String PositionStartDate)
    {
        this.PositionStartDate = PositionStartDate;
    }

    public UserArea getUserArea ()
    {
        return UserArea;
    }

    public void setUserArea (UserArea UserArea)
    {
        this.UserArea = UserArea;
    }

    public JobCategory[] getJobCategory ()
    {
        return JobCategory;
    }

    public void setJobCategory (JobCategory[] JobCategory)
    {
        this.JobCategory = JobCategory;
    }

    public String getDepartmentName ()
    {
        return DepartmentName;
    }

    public void setDepartmentName (String DepartmentName)
    {
        this.DepartmentName = DepartmentName;
    }

    public JobGrade[] getJobGrade ()
    {
        return JobGrade;
    }

    public void setJobGrade (JobGrade[] JobGrade)
    {
        this.JobGrade = JobGrade;
    }

    public String getPositionURI ()
    {
        return PositionURI;
    }

    public void setPositionURI (String PositionURI)
    {
        this.PositionURI = PositionURI;
    }

    public String getPublicationStartDate ()
    {
        return PublicationStartDate;
    }

    public void setPublicationStartDate (String PublicationStartDate)
    {
        this.PublicationStartDate = PublicationStartDate;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [OrganizationName = "+OrganizationName+", PositionLocationDisplay = "+PositionLocationDisplay+", PositionTitle = "+PositionTitle+", PositionSchedule = "+PositionSchedule+", ApplicationCloseDate = "+ApplicationCloseDate+", PositionID = "+PositionID+", PositionEndDate = "+PositionEndDate+", PositionOfferingType = "+PositionOfferingType+", PositionRemuneration = "+PositionRemuneration+", ApplyURI = "+ApplyURI+", PositionFormattedDescription = "+PositionFormattedDescription+", PositionLocation = "+PositionLocation+", QualificationSummary = "+QualificationSummary+", PositionStartDate = "+PositionStartDate+", UserArea = "+UserArea+", JobCategory = "+JobCategory+", DepartmentName = "+DepartmentName+", JobGrade = "+JobGrade+", PositionURI = "+PositionURI+", PublicationStartDate = "+PublicationStartDate+"]";
    }
}
