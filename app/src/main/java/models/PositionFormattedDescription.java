package models;

public class PositionFormattedDescription
{
    private String LabelDescription;

    private String Label;

    public String getLabelDescription ()
    {
        return LabelDescription;
    }

    public void setLabelDescription (String LabelDescription)
    {
        this.LabelDescription = LabelDescription;
    }

    public String getLabel ()
    {
        return Label;
    }

    public void setLabel (String Label)
    {
        this.Label = Label;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [LabelDescription = "+LabelDescription+", Label = "+Label+"]";
    }
}