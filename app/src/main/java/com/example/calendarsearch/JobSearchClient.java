package com.example.calendarsearch;

import models.RootObject;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface JobSearchClient {
    @Headers("Authorization-Key: SGXAgo8h5BwDrrpLLOUAR66p2Us68ngUm/wxQCLBtMc=")
    @GET("search")
    Call<RootObject> getJobs(@Query("Keyword") String keyword);
}
