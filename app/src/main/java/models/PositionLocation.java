package models;

public class PositionLocation
{
    private String CountrySubDivisionCode;

    private String Latitude;

    private String CityName;

    private String CountryCode;

    private String Longitude;

    private String LocationName;

    public String getCountrySubDivisionCode ()
    {
        return CountrySubDivisionCode;
    }

    public void setCountrySubDivisionCode (String CountrySubDivisionCode)
    {
        this.CountrySubDivisionCode = CountrySubDivisionCode;
    }

    public String getLatitude ()
    {
        return Latitude;
    }

    public void setLatitude (String Latitude)
    {
        this.Latitude = Latitude;
    }

    public String getCityName ()
    {
        return CityName;
    }

    public void setCityName (String CityName)
    {
        this.CityName = CityName;
    }

    public String getCountryCode ()
    {
        return CountryCode;
    }

    public void setCountryCode (String CountryCode)
    {
        this.CountryCode = CountryCode;
    }

    public String getLongitude ()
    {
        return Longitude;
    }

    public void setLongitude (String Longitude)
    {
        this.Longitude = Longitude;
    }

    public String getLocationName ()
    {
        return LocationName;
    }

    public void setLocationName (String LocationName)
    {
        this.LocationName = LocationName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [CountrySubDivisionCode = "+CountrySubDivisionCode+", Latitude = "+Latitude+", CityName = "+CityName+", CountryCode = "+CountryCode+", Longitude = "+Longitude+", LocationName = "+LocationName+"]";
    }
}