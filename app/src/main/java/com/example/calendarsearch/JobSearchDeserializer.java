package com.example.calendarsearch;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import models.RootObject;

public class JobSearchDeserializer {
    public static GsonBuilder createGsonBuilder() {
        JsonDeserializer<RootObject> deserializer = new JsonDeserializer<RootObject>() {
            @Override
            public RootObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                    throws JsonParseException {
                RootObject rootObject = null;
                try {

                    if (json != null) {
                        Gson gson = new Gson();
                        String jsonString = json.toString();
                        if (isNullOrEmpty(jsonString)) {
                            throw new Exception("JobSearchDeserializer: deserialization failed: jsonString isNullOrEmpty");
                        }
                        jsonString = jsonString.replace("{}", "null");
                        rootObject = gson.fromJson(jsonString, RootObject.class);

                        if (rootObject == null) {
                            throw new Exception("JobSearchDeserializer: deserialization failed.");
                        }
                    }
                } catch (Exception e) {
                    System.out.println(e.toString());
                }

                return rootObject;
            }
        };

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(RootObject.class, deserializer);

        return gsonBuilder;
    }

    public static boolean isNullOrEmpty(String str) {
        if (str != null && !str.isEmpty())
            return false;
        return true;
    }
}
