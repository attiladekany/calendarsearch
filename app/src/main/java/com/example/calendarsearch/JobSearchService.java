package com.example.calendarsearch;

import com.google.gson.Gson;

import models.RootObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class JobSearchService {
    private final String BASE_URL = "https://data.usajobs.gov/api/";
    private final Retrofit retrofit;

    public JobSearchService() {
        this.retrofit = buildRetrofit();
    }

    public void getJobs(Callback<RootObject> callback) {
        // Todo: this value should be dynamic...
        String keyword = "Software";
        Call<RootObject> call = retrofit.create(JobSearchClient.class).getJobs(keyword);
        call.enqueue(callback);
    }

    private Retrofit buildRetrofit() {
        Gson gson = JobSearchDeserializer.createGsonBuilder()
                .create();

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }
}
