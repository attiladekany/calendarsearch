package com.example.calendarsearch;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

class JobViewHolder extends RecyclerView.ViewHolder {
    TextView PositionID;
    TextView PositionTitle;
    TextView PositionLocationDisplay;
    TextView JobCategory_Name;
    TextView PositionSchedule_Name;
    TextView PositionOfferingType;
    TextView PositionRemuneration_MinimumRange;
    TextView PositionRemuneration_MaximumRange;

    JobViewHolder(View itemView) {
        super(itemView);
        PositionID = itemView.findViewById(R.id.PositionID);
        PositionTitle = itemView.findViewById(R.id.PositionTitle);
        JobCategory_Name = itemView.findViewById(R.id.JobCategory_Name);
        PositionLocationDisplay = itemView.findViewById(R.id.PositionLocationDisplay);
        PositionOfferingType = itemView.findViewById(R.id.PositionOfferingType);
        PositionSchedule_Name = itemView.findViewById(R.id.PositionSchedule_Name);
        PositionRemuneration_MinimumRange = itemView.findViewById(R.id.MinMoney);
        PositionRemuneration_MaximumRange = itemView.findViewById(R.id.MaxMoney);
    }
}