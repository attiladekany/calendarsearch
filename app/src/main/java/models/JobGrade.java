package models;

public class JobGrade
{
    private String Code;

    public String getCode ()
    {
        return Code;
    }

    public void setCode (String Code)
    {
        this.Code = Code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Code = "+Code+"]";
    }
}
