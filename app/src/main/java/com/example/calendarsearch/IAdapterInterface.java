package com.example.calendarsearch;

import java.util.ArrayList;

import models.SearchResultItems;

public interface IAdapterInterface<T> {
     public void updateAdapter(ArrayList<SearchResultItems> searchResultItemList);
}
