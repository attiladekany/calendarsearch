package com.example.calendarsearch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.SearchView;

import java.util.ArrayList;

import models.RootObject;
import models.SearchResultItems;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private JobAdapter adapter;
    private ArrayList<SearchResultItems> jobList;

    // Api reference:
    // https://developer.usajobs.gov/API-Reference/GET-api-Search
    String URL = "https://data.usajobs.gov/api/search";

    private JobSearchService jobSearchService;
    private JobSearchResponseMapper mapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        jobSearchService = new JobSearchService();
        mapper = new JobSearchResponseMapper();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initAdapter();
        setUpRecyclerView();
        getJobList();
    }

    public void getJobList() {
        jobSearchService.getJobs(new Callback<RootObject>() {
            @Override
            public void onResponse(Call<RootObject> call, Response<RootObject> response) {
                jobList = (ArrayList<SearchResultItems>) mapper.mapToSearchItems(response.body());
                adapter.updateAdapter(jobList);
            }

            @Override
            public void onFailure(Call<RootObject> call, Throwable t) {
                Log.e("MainActivity", t.getMessage());
                jobList = null;
            }
        });
    }


    private void initAdapter() {
        jobList = new ArrayList<>();
        adapter = new JobAdapter(jobList);
    }

    private void setUpRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        adapter = new JobAdapter(jobList);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }

}