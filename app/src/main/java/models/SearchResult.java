package models;

public class SearchResult
{
    private String SearchResultCountAll;

    private SearchResultItems[] SearchResultItems;

    private UserArea UserArea;

    private String SearchResultCount;

    public String getSearchResultCountAll ()
    {
        return SearchResultCountAll;
    }

    public void setSearchResultCountAll (String SearchResultCountAll)
    {
        this.SearchResultCountAll = SearchResultCountAll;
    }

    public SearchResultItems[] getSearchResultItems ()
    {
        return SearchResultItems;
    }

    public void setSearchResultItems (SearchResultItems[] SearchResultItems)
    {
        this.SearchResultItems = SearchResultItems;
    }

    public UserArea getUserArea ()
    {
        return UserArea;
    }

    public void setUserArea (UserArea UserArea)
    {
        this.UserArea = UserArea;
    }

    public String getSearchResultCount ()
    {
        return SearchResultCount;
    }

    public void setSearchResultCount (String SearchResultCount)
    {
        this.SearchResultCount = SearchResultCount;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [SearchResultCountAll = "+SearchResultCountAll+", SearchResultItems = "+SearchResultItems+", UserArea = "+UserArea+", SearchResultCount = "+SearchResultCount+"]";
    }
}