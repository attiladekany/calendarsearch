package models;

public class UserArea
{
    private Details Details;

    private String IsRadialSearch;

    public Details getDetails ()
    {
        return Details;
    }

    public void setDetails (Details Details)
    {
        this.Details = Details;
    }

    public String getIsRadialSearch ()
    {
        return IsRadialSearch;
    }

    public void setIsRadialSearch (String IsRadialSearch)
    {
        this.IsRadialSearch = IsRadialSearch;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Details = "+Details+", IsRadialSearch = "+IsRadialSearch+"]";
    }
}