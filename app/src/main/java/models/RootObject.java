package models;
public class RootObject
{
    private String LanguageCode;

    private String SearchParameters;

    private SearchResult SearchResult;

    public String getLanguageCode ()
    {
        return LanguageCode;
    }

    public void setLanguageCode (String LanguageCode)
    {
        this.LanguageCode = LanguageCode;
    }

    public String getSearchParameters ()
    {
        return SearchParameters;
    }

    public void setSearchParameters (String SearchParameters)
    {
        this.SearchParameters = SearchParameters;
    }

    public SearchResult getSearchResult ()
    {
        return SearchResult;
    }

    public void setSearchResult (SearchResult SearchResult)
    {
        this.SearchResult = SearchResult;
    }

    @Override
    public String toString()
    {
        return "RootObject [LanguageCode = "+LanguageCode+", SearchParameters = "+SearchParameters+", SearchResult = "+SearchResult+"]";
    }
}
